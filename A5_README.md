> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

### Assignment 5 Requirements:

* Set up our online portfolio
* Set up our A5 page and create a workable interface
* Add Server-side validation to the interfaces.
*Reviewing over sub-directories

#### README.md file should include the following items:

* Screenshot of A5's functioning interface
* Screenshot of error page on add_petstore_processes.php

#### Assignment Screenshots:

*Screenshot of A5 :

![A5 Page](img/a5_1.png)

*Screenshots of error on add_petstore_process.php:

![Empty Fields](img/a5_2.png)
