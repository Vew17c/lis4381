> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

### Assignment 4 Requirements:

* Set up our online portfolio
* Set up our A4 page and create a workable interface
* Add JQery validation to the interface.s

#### README.md file should include the following items:

* Screenshot of my homepage
* Screenshot of A4's functioning interface

#### Assignment Screenshots:

*Screenshot of the database ERD*:

![Homepage](img/a4_1.png)

*Screenshots of my portfolio's homepage*:

![Empty Fields](img/a4_2.png)

*Screenshots of my A4 page with zero-filled entries*:

![Filled Fields](img/a4_3.png)

