> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

### Assignment 3 Requirements:

* Create an app that calculates the total prices for concert tickets.
* Create a database in MySQL Workbench.

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application�s first user interface
* Screenshot of running application�s second user interface
* Links to a3.mwb and a3.sql 

#### Assignment Screenshots:

*Screenshot of the database ERD*:

![a3_ERD](img/a3_ERD.png)

*Screenshots of the MyEvent first user interface*:

![MyEvent 1](img/myevent1.png)

*Screenshots of the MyEvent second user interface*:

![MyEvent 2](img/myevent2.png)

*Links to a3.mwb and a3.sql*:

[a3.mwb](fls/a3wb_vincent_williams.mwb)

[a3.sql](fls/a3sql_vincent_williams.sql)